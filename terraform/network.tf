
# Module to create a Local VPC
## Docs: https://github.com/terraform-google-modules/terraform-google-network
module "network" {
  source     = "terraform-google-modules/network/google"
  version    = "~> 3.4"
  depends_on = [google_project_service.apis]

  project_id   = var.gcp_project_id
  network_name = var.gcp_project_name
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name   = "compute-subnet"
      subnet_ip     = "10.10.10.0/28"
      subnet_region = var.region
    },
  ]
}

# # Module to create a Serverless Load Balancing
# ## Docs: https://cloud.google.com/blog/topics/developers-practitioners/new-terraform-module-serverless-load-balancing
# # https://cloud.google.com/blog/topics/developers-practitioners/serverless-load-balancing-terraform-hard-way

module "lb-https" {
  source     = "terraform-google-modules/terraform-google-lb-http/tree/master/modules/dynamic_backends"
  version    = "~> 6.1" // https://github.com/terraform-google-modules/terraform-google-lb-http/releases
  depends_on = [google_project_service.apis, google_iap_client.iap_client]

  project                         = var.gcp_project_id
  name                            = "lb-nginx"
  ssl                             = true
  managed_ssl_certificate_domains = [var.domain]
  https_redirect                  = true
  backends = {
    default = {
      description             = null
      enable_cdn              = true
      custom_request_headers  = null
      custom_response_headers = null
      security_policy         = null

      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      health_check = {
        request_path        = "/"
        check_interval_sec  = 10
        timeout_sec         = 10
        healthy_threshold   = 2
        unhealthy_threshold = 10
      }
      groups = [
        {
          group = google_compute_instance_group_manager.nginx
        }
      ]

      iap_config = {
        enable               = true
        oauth2_client_id     = google_iap_client.iap_client.client_id
        oauth2_client_secret = google_iap_client.iap_client.secret
      }
    }
  }
}

resource "google_iap_brand" "iap_brand" {
  support_email     = "bbrasil@gmail.com"
  application_title = "Cloud IAP protected Nginx"
  project           = var.gcp_project_id
}

resource "google_iap_client" "iap_client" {
  display_name = "The IAP Client"
  brand        = google_iap_brand.iap_brand.name
}
