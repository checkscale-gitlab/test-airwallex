## Project Introduction

This project proposes to create an instance group, using a secure linux image generated in [Packer](https://www.packer.io/), and implement an Nginx in it running by Docker Composer. This service is being exposed by a Global LB, protected by the [Identity Aware Proxy](https://cloud.google.com/blog/topics/developers-practitioners/control-access-your-web-sites-identity-aware-proxy).

## Architecture Overview

This is the prospect of an environment

![Architecture Overview](architecture.png)

### TODO List

[ ] - Create a Script to hardering nginx docker image.

[ ] - Create a gitlab-ci.yml to automate a CI/CD flow, and

[ ] - use the Cloud Build to create the images.
