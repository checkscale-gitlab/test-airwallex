
variable "region" {
  type    = string
  default = "europe-west4"
}

variable "zone" {
  type    = string
  default = "europe-west4-a"
}

variable "gcp_project_id" {
  type        = string
  description = "GCP project id that will be used"
  default     = "asymmetric-ray-328309"
}

variable "gcp_credentials_json" {
  type        = string
  description = "File for access to GCP Project"
  default     = "./key.json"
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }
