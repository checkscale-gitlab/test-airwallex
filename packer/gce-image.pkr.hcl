
# source blocks are generated from your builders; a source can be referenced in
# build blocks. A build block runs provisioners and post-processors on a
# source.
source "googlecompute" "gce_image" {
  project_id          = var.gcp_project_id
  image_name          = "nginx-harder"
  zone                = var.zone
  source_image        = "ubuntu-2004-focal-v20210927"
  source_image_family = "ubuntu-2004-lts"
  ssh_username        = "packer"
  tags                = ["packer", "nginx"]
}

# a build block invokes sources and runs provisioning steps on them.
build {
  sources = ["sources.googlecompute.gce_image"]

  provisioner "file" {
    source      = "./scripts/harden.sh"
    destination = "~/harden.sh"
  }

  provisioner "shell" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install docker.io -y",
      "sudo curl -L 'https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-$(uname -s)-$(uname -m)' -o /usr/local/bin/docker-compose"
    ]
  }

  provisioner "shell" {
    inline = [
      "sudo bash ~/harden.sh",
      "rm ~/harden.sh"
    ]
    remote_folder = "~"
  }
}
