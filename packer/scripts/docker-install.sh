#!/bin/bash
sudo apt-get update
sudo apt-get install docker.io curl -y
sudo curl -L "https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
