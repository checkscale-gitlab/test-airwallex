source "docker" "docker_image" {
  image  = "nginx:1.21.3-alpine"
  commit = true
}

build {
  sources = ["source.docker.docker_image"]

  # Fix CVE-2021-22945, CVE-2021-22946, CVE-2021-22947, CVE-2021-22945, CVE-2021-22946 and CVE-2021-22947
  provisioner "shell" {
    inline = [
      "apk add --no-cache 'curl>=7.79.0-r0' 'libcurl>=7.79.0-r0'",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "gcr.io/${var.gcp_project_id}/nginx"
      tags       = ["1.21.3-harder"]
    }

    post-processor "docker-push" {
      login          = true
      login_username = "_json_key"
      login_password = file("./key.json")
      login_server   = "https://gcr.io/"
    }
  }
}


